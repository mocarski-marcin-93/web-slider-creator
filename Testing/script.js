function Stack(size) {
    this.stack = new Array(size);
    this.size = 0;
    this.maxSize = size;
}
Stack.prototype.isEmpty = function() {
    return (this.size === 0) ? true : false;
};
Stack.prototype.isFull = function() {
    return (this.size === this.maxSize) ? true : false;
};
Stack.prototype.push = function(element) {
    this.stack[this.size] = element;
    this.size++;
    return true;
};
Stack.prototype.pull = function() {
    return (!this.isEmpty()) ? this.stack[--this.size] : false;
};

var imgsN = 20;
var imgsTab = new Array(imgsN);
for (var i = 0; i < imgsN; i++) {
    imgsTab[i] = "src/img/img.png";
}
var showImgsNumber = 6;
var sliderWidth = 1200;
var sliderHeight = 300;
$(document).ready(function() {
    var slider = document.getElementById("imagesDiv");
    var slideLeft = document.getElementById("arrowLeft");
    var slideRight = document.getElementById("arrowRight");
    var stack = new Stack(imgsN - showImgsNumber);
    //--Przesuwanie w lewo
    var slideLeftF = function() {
        if (!stack.isFull()) {
            stack.push();
            $(".imagesDiv").animate({
                left: "-=" + ((sliderWidth - 100) / showImgsNumber) + "px"
            }, 500, "linear", function() {
                $(slideLeft).one("click", slideLeftF);
            });
        } else {
            $(".imagesDiv").animate({
                left: "-=" + ((sliderWidth - 100) / showImgsNumber) / 10 + "px"
            }, 100, "linear", function() {
                $(".imagesDiv").animate({
                    left: "+=" + ((sliderWidth - 100) / showImgsNumber) / 10 + "px"
                }, 100, "linear", function() {
                    $(slideLeft).one("click", slideLeftF);
                });
            });
        }
    }
    $(slideLeft).one("click", slideLeftF);
    //--Przesuwanie w prawo
    var slideRightF = function(e) {
        if (!stack.isEmpty()) {
            stack.pull();
            $(".imagesDiv").animate({
                left: "+=" + ((sliderWidth - 100) / showImgsNumber) + "px"
            }, 500, "linear", function() {
                $(slideRight).one("click", slideRightF);
            });
        } else {
            $(".imagesDiv").animate({
                left: "+=" + ((sliderWidth - 100) / showImgsNumber) / 10 + "px"
            }, 100, "linear", function() {
                $(".imagesDiv").animate({
                    left: "-=" + ((sliderWidth - 100) / showImgsNumber) / 10 + "px"
                }, 100, "linear", function() {
                    $(slideRight).one("click", slideRightF);
                });
            });
        }
    }
    $(slideRight).one("click", slideRightF);
});
