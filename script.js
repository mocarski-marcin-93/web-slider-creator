//https://jsfiddle.net/gxbr1nyb/10/

/*
---!--WYMAGANIA:
---!  Slider trzyma tylko obrazki!
---!  Potrzebne nam tylko i wyłącznie przesuwanie w lewo/prawo
---! Szerokość podana bez jednostek
---!--USTAWIENIA:
---!  whereToAppend        - obiekt DOMHtml do ktorego wrzucamy Slider
---!  imgsTab              - tablica ściezek obrazkow
---!  imgsNumber           - ilość obrazkow
---!  showImgsNumber       - ilość obrazkow widoczna
---!  sliderWidth          - szerokość Slidera
---!  sliderHeight         - wysokość Slidera
---!
*/

var whereToAppend = document.getElementById("sectionId");

function Stack(size) {
    this.stack = new Array(size);
    this.size = 0;
    this.maxSize = size;
}
Stack.prototype.isEmpty = function() {
    return (this.size === 0) ? true : false;
};
Stack.prototype.isFull = function() {
    return (this.size === this.maxSize) ? true : false;
};
Stack.prototype.push = function(element) {
    this.stack[this.size] = element;
    this.size++;
    return true;
};
Stack.prototype.pull = function() {
    return (!this.isEmpty()) ? this.stack[--this.size] : false;
};

function Slider(whereToAppend, imgsTab, imgsNumber, showImgsNumber, sliderWidth, sliderHeight) {
    if (imgsTab.length !== imgsNumber || imgsNumber < showImgsNumber || imgsNumber <= 0 || showImgsNumber <= 0) throw "ERROR: Podano złą liczbę obrazkow.";
    this.self = this;
    this.widthArrows = 50;
    this.where = whereToAppend;
    this.imgsN = imgsNumber;
    this.showN = showImgsNumber;
    this.imgsTab = imgsTab;
    this.sliderWidth = sliderWidth;
    this.sliderHeight = sliderHeight;
    this.stack = new Stack(this.imgsN - this.showN);

    this.toHtml = "";
    this.divStack = new Stack(50);

    this.toJs = "          function Stack(size) {\n" +
"                  this.stack = new Array(size);\n" +
"                  this.size = 0;\n" +
"                  this.maxSize = size;\n" +
"              }\n" +
"              Stack.prototype.isEmpty = function() {\n" +
"                  return (this.size === 0) ? true : false;\n" +
"              };\n" +
"              Stack.prototype.isFull = function() {\n" +
"                  return (this.size === this.maxSize) ? true : false;\n" +
"              };\n" +
"              Stack.prototype.push = function(element) {\n" +
"                  this.stack[this.size] = element;\n" +
"                  this.size++;\n" +
"                  return true;\n" +
"              };\n" +
"              Stack.prototype.pull = function() {\n" +
"                  return (!this.isEmpty()) ? this.stack[--this.size] : false;\n" +
"              };\n";

    this.slider = null;
    this.createWrapperDiv(); //ta funkcja ustawi this.slider, poda głowny kontener

    this.wrapperInner = null;
    this.addArrowsAndWrapperInner(); //ta funkcja ustawi this.wrapperInner, poda "ramke" do wyswietlania w niej contentu

    this.imagesDiv = null;
    this.addImagesDiv(); //ta funkcja ustawi this.imagesDiv, poda diva zawierającego content

    this.imagesWrappers = new Array(this.imgsN);
    this.addImagesWrappers(); // ta funkcja wypełni this.imagesWrappers[], poda divy zawierające obrazki
    this.where.appendChild(this.slider);
    this.addArrowsListeners();

    while (!this.divStack.isEmpty()) {
        this.toHtml += this.divStack.pull();
    }
    document.getElementById("resultHtml").innerHTML = "<xmp>\n" + this.toHtml + "</xmp>";
    this.createJS();
    document.getElementById("resultJs").innerHTML = this.toJs;
}

Slider.prototype.createWrapperDiv = function() {
    var wrapperDiv = document.createElement("DIV");
    wrapperDiv.setAttribute("class", "wrapperDiv");
    wrapperDiv.style.width = this.sliderWidth;
    wrapperDiv.style.height = this.sliderHeight;
    this.slider = wrapperDiv;
    // to HTML
    this.toHtml += "        <div class='wrapperDiv' style = 'width:" + this.sliderWidth + "px; height:" + this.sliderHeight + "px;'>\n";
    this.divStack.push("\n        </div>");
};

Slider.prototype.addArrowsAndWrapperInner = function() {
    //strzałka w lewo
    var arrowLeft = document.createElement("DIV");
    arrowLeft.setAttribute("class", "arrowLeft");
    arrowLeft.setAttribute("id", "arrowLeft");
    this.slider.appendChild(arrowLeft);
    // to HTML
    this.toHtml += "          <div class='arrowLeft' id='arrowLeft'></div>";
    //okno slidera
    var wrapperInnerDiv = document.createElement("DIV");
    wrapperInnerDiv.setAttribute("class", "wrapperInnerDiv");
    wrapperInnerDiv.style.width = (this.sliderWidth - 100) + "px";
    wrapperInnerDiv.style.height = (this.sliderHeight - 50) + "px";
    this.slider.appendChild(wrapperInnerDiv);
    this.wrapperInner = wrapperInnerDiv;
    // to HTML
    this.toHtml += "\n          <div class='wrapperInnerDiv' style='width: " + (this.sliderWidth - 100) + "px; height:" + (this.sliderHeight - 50) + "px;'>\n";
    //strzałka w prawo
    var arrowRight = document.createElement("DIV");
    arrowRight.setAttribute("class", "arrowRight");
    arrowRight.setAttribute("id", "arrowRight");
    this.slider.appendChild(arrowRight);
    this.divStack.push("\n          </div>\n          <div class='arrowRight' id='arrowRight'></div>")
};

Slider.prototype.addArrowsListeners = function() {
    var that = this.self;
    $(document).ready(function() {
        var slider = document.getElementById("imagesDiv");
        var slideLeft = document.getElementById("arrowLeft");
        var slideRight = document.getElementById("arrowRight");

        //--Przesuwanie w lewo
        var slideLeftF = function() {
            if (!that.stack.isFull()) {
                that.stack.push();
                $(".imagesDiv").animate({
                    left: "-=" + ((that.sliderWidth - 100) / that.showN) + "px"
                }, 500, 'linear', function() {
                    $(slideLeft).one("click", slideLeftF);
                });
            } else {
                $('.imagesDiv').animate({
                    left: "-=" + ((that.sliderWidth - 100) / that.showN) / 10 + "px"
                }, 100, 'linear', function() {
                    $('.imagesDiv').animate({
                        left: "+=" + ((that.sliderWidth - 100) / that.showN) / 10 + "px"
                    }, 100, 'linear', function() {
                        $(slideLeft).one("click", slideLeftF);
                    });
                });
            }
        }
        $(slideLeft).one("click", slideLeftF);

        //--Przesuwanie w prawo
        var slideRightF = function(e) {
            if (!that.stack.isEmpty()) {
                that.stack.pull();
                $('.imagesDiv').animate({
                    left: "+=" + ((that.sliderWidth - 100) / that.showN) + "px"
                }, 500, 'linear', function() {
                    $(slideRight).one("click", slideRightF);
                });
            } else {
                $('.imagesDiv').animate({
                    left: "+=" + ((that.sliderWidth - 100) / that.showN) / 10 + "px"
                }, 100, 'linear', function() {
                    $('.imagesDiv').animate({
                        left: "-=" + ((that.sliderWidth - 100) / that.showN) / 10 + "px"
                    }, 100, 'linear', function() {
                        $(slideRight).one("click", slideRightF);
                    });
                });
            }
        }
        $(slideRight).one("click", slideRightF);
    });
};

Slider.prototype.addImagesDiv = function() {
    var newImagesDiv = document.createElement("DIV");
    newImagesDiv.style.width = (this.sliderWidth - 100) / this.showN * this.imgsN + "px";
    newImagesDiv.style.height = (this.sliderHeight - 50) + "px";
    newImagesDiv.setAttribute("class", "imagesDiv");
    this.wrapperInner.appendChild(newImagesDiv);
    this.imagesDiv = newImagesDiv;
    // to HTML
    this.toHtml += "            <div class='imagesDiv' style='width:" + (this.sliderWidth - 100) / this.showN * this.imgsN + "px; height:" + (this.sliderHeight - 50) + "px;'>\n";
    this.divStack.push("            </div>");
};

Slider.prototype.addImagesWrappers = function() {
    for (var i = 0; i < this.imgsN; i++) {
        var newImgWrapper = document.createElement("DIV");
        newImgWrapper.style.width = (this.sliderWidth - 100) / this.showN + "px";
        newImgWrapper.style.height = (this.sliderHeight - 50) + "px";
        // to HTML
        this.toHtml += "\t\t\t<div class='imgWrapper' style='width:" + (this.sliderWidth - 100) / this.showN + "px; height:" + (this.sliderHeight - 50) + "px;'>\n";

        var newImg = document.createElement("IMG");
        newImg.setAttribute("src", this.imgsTab[i]);
        newImg.setAttribute("class", "innerImg");
        newImg.style.width = ((this.sliderWidth - 100) / this.showN - 10) + "px";
        newImgWrapper.appendChild(newImg);
        newImgWrapper.setAttribute("class", "imgWrapper");
        this.imagesDiv.appendChild(newImgWrapper);
        // to HTML
        this.toHtml += "\t\t\t\t<img src='" + this.imgsTab[i] + "' class='innerImg' style='width:" + ((this.sliderWidth - 100) / this.showN - 10) + "px;'>\n";
        this.toHtml += "\t\t\t</div>\n";
    }
};

Slider.prototype.createJS = function() {
    this.toJs += ('\n          var imgsN =' + this.imgsN + ';\n' +
        '          var imgsTab = new Array(imgsN);\n' +
        '          for(var i = 0; i < imgsN; i++) {              \n              imgsTab[i] = "src/img/img.png";\n          }\n' +
        '          var showImgsNumber =' + this.showN + ';\n' +
        '          var sliderWidth = ' + this.sliderWidth + ';\n' +
        '          var sliderHeight = ' + this.sliderHeight + ';\n' +
        '           $(document).ready(function() {\n' +
        '             var slider = document.getElementById("imagesDiv");\n' +
        '             var slideLeft = document.getElementById("arrowLeft");\n' +
        '             var slideRight = document.getElementById("arrowRight");\n' +
        '             var stack = new Stack(imgsN - showImgsNumber);\n' +
        '             //--Przesuwanie w lewo\n' +
        '             var slideLeftF = function() {\n' +
        '             if (!stack.isFull()) {\n' +
        '              stack.push();\n' +
        '              $(".imagesDiv").animate({\n' +
        '                left: "-=" + ((sliderWidth - 100) / showImgsNumber) + "px"\n' +
        '              }, 500, "linear", function() {\n' +
        '                $(slideLeft).one("click", slideLeftF);\n' +
        '              });\n' +
        '             } else {\n' +
        '               $(".imagesDiv").animate({\n' +
        '                 left: "-=" + ((sliderWidth - 100) / showImgsNumber) / 10 + "px"\n' +
        '               }, 100, "linear", function() {\n' +
        '                 $(".imagesDiv").animate({\n' +
        '                   left: "+=" + ((sliderWidth - 100) / showImgsNumber) / 10 + "px"\n' +
        '                 }, 100, "linear", function() {\n' +
        '                   $(slideLeft).one("click", slideLeftF);\n' +
        '                 });\n' +
        '               });\n' +
        '              }\n' +
        '             }\n' +
        '             $(slideLeft).one("click", slideLeftF);\n' +
        '             //--Przesuwanie w prawo\n' +
        '             var slideRightF = function(e) {\n' +
        '               if (!stack.isEmpty()) {\n' +
        '                 stack.pull();\n' +
        '                 $(".imagesDiv").animate({\n' +
        '                   left: "+=" + ((sliderWidth - 100) / showImgsNumber) + "px"\n' +
        '                 }, 500, "linear", function() {\n' +
        '                   $(slideRight).one("click", slideRightF);\n' +
        '                 });\n' +
        '               } else {\n' +
        '                 $(".imagesDiv").animate({\n' +
        '                   left: "+=" + ((sliderWidth - 100) / showImgsNumber) / 10 + "px"\n' +
        '                 }, 100, "linear", function() {\n' +
        '                   $(".imagesDiv").animate({\n' +
        '                     left: "-=" + ((sliderWidth - 100) / showImgsNumber) / 10 + "px"\n' +
        '                   }, 100, "linear", function() {\n' +
        '                   $(slideRight).one("click", slideRightF);\n' +
        '                   });\n' +
        '                 });\n' +
        '               }\n' +
        '             }\n' +
        '             $(slideRight).one("click", slideRightF);\n' +
        '           });');
};





{
    var inputWidth = document.getElementById("width");
    var inputWidthInc = document.getElementById("widthInc");
    var inputWidthDec = document.getElementById("widthDec");
    inputWidthInc.addEventListener("click", function() {
        inputWidth.value = inputWidth.value * 1 + 10;
    });
    inputWidthDec.addEventListener("click", function() {
        inputWidth.value = (inputWidth.value > 0) ? (inputWidth.value * 1 - 10) : (inputWidth.value);
    });

    var inputheight = document.getElementById("height");
    var inputheightInc = document.getElementById("heightInc");
    var inputheightDec = document.getElementById("heightDec");
    inputheightInc.addEventListener("click", function() {
        inputheight.value = inputheight.value * 1 + 10;
    });
    inputheightDec.addEventListener("click", function() {
        inputheight.value = (inputheight.value > 0) ? (inputheight.value * 1 - 10) : (inputheight.value);
    });

    var inputimgsNumber = document.getElementById("imgsNumber");
    var inputimgsNumberInc = document.getElementById("imgsNumberInc");
    var inputimgsNumberDec = document.getElementById("imgsNumberDec");
    inputimgsNumberInc.addEventListener("click", function() {
        inputimgsNumber.value = 1 * inputimgsNumber.value + 1;
    });
    inputimgsNumberDec.addEventListener("click", function() {
        inputimgsNumber.value = (inputimgsNumber.value > 0) ? (1 * inputimgsNumber.value - 1) : (inputimgsNumber.value);
    });

    var inputimgsShowNumber = document.getElementById("imgsShowNumber");
    var inputimgsShowNumberInc = document.getElementById("imgsShowNumberInc");
    var inputimgsShowNumberDec = document.getElementById("imgsShowNumberDec");
    inputimgsShowNumberInc.addEventListener("click", function() {
        inputimgsShowNumber.value = 1 * inputimgsShowNumber.value + 1;
    });
    inputimgsShowNumberDec.addEventListener("click", function() {
        inputimgsShowNumber.value = (inputimgsShowNumber.value > 0) ? (1 * inputimgsShowNumber.value - 1) : (inputimgsShowNumber.value);
    });
    var okej = document.getElementById("ok");
    okej.addEventListener("click", function() {
        var targetSection = document.getElementById("sectionId");
        $(targetSection).empty();
        var imgsN = 1 * inputimgsNumber.value;
        var imgsTab = new Array(imgsN);
        for (var i = 0; i < imgsN; i++) {
            imgsTab[i] = "src/img/img.png";
        }
        var showImgsNumber = 1 * inputimgsShowNumber.value;
        var sliderWidth = 1 * inputWidth.value;
        var sliderHeight = 1 * inputheight.value;
        var slider = new Slider(targetSection, imgsTab, imgsN, showImgsNumber, sliderWidth, sliderHeight);
    });
}
